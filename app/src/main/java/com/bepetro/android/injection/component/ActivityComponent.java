package com.bepetro.android.injection.component;

import com.bepetro.android.injection.PerActivity;
import com.bepetro.android.injection.module.ActivityModule;
import com.bepetro.android.ui.LauncherActivity;
import com.bepetro.android.ui.favorite.FavoriteFragment;
import com.bepetro.android.ui.feed.ActivityPostDetails;
import com.bepetro.android.ui.feed.FeedFragment;
import com.bepetro.android.ui.main.MainActivity;
import com.bepetro.android.ui.signin.SignInActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SignInActivity signInActivity);

    void inject(LauncherActivity launcherActivity);

    void inject(MainActivity mainActivity);

    void inject(com.bepetro.android.ui.favorite.ActivityPostDetails postDetails);

    void inject(ActivityPostDetails postDetails);

    void inject(FeedFragment feedFragment);

    void inject(FavoriteFragment favoriteFragment);
}

