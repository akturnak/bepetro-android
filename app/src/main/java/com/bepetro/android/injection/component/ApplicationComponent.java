package com.bepetro.android.injection.component;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;

import com.bepetro.android.data.remote.BePetroService;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import com.bepetro.android.BePetroApplication;
import com.bepetro.android.data.DataManager;
import com.bepetro.android.data.local.DatabaseHelper;
import com.bepetro.android.data.local.PreferencesHelper;
import com.bepetro.android.data.remote.UnauthorisedInterceptor;
import com.bepetro.android.injection.ApplicationContext;
import com.bepetro.android.injection.module.ApplicationModule;
import com.bepetro.android.service.PostsSyncService;
import com.bepetro.android.service.BootCompletedReceiver;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BePetroApplication bePetroApplication);
    void inject(UnauthorisedInterceptor unauthorisedInterceptor);
    void inject(PostsSyncService postsSyncService);
    void inject(BootCompletedReceiver bootCompletedReceiver);
    void inject(DataManager dataManager);

    @ApplicationContext Context context();
    Application application();
    BePetroService ribotService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    Bus eventBus();
    AccountManager accountManager();
}
