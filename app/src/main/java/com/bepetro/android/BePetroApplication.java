package com.bepetro.android;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import com.bepetro.android.data.BusEvent;
import com.bepetro.android.data.DataManager;
import com.bepetro.android.injection.component.ApplicationComponent;
import com.bepetro.android.injection.component.DaggerApplicationComponent;
import com.bepetro.android.injection.module.ApplicationModule;
import com.bepetro.android.ui.signin.SignInActivity;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class BePetroApplication extends Application {

    @Inject Bus mEventBus;
    @Inject DataManager mDataManager;
    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        Fresco.initialize(this);

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);
        mEventBus.register(this);
    }

    public static BePetroApplication get(Context context) {
        return (BePetroApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    @Subscribe
    public void onAuthenticationError(BusEvent.AuthenticationError event) {
        mDataManager.signOut()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        startSignInActivity();
                    }
                });
    }

    private void startSignInActivity() {
        startActivity(SignInActivity.getStartIntent(
                this, true, getString(R.string.authentication_message)));
    }
}

