package com.bepetro.android.ui.feed;

import java.util.List;

import com.bepetro.android.data.model.Post;
import com.bepetro.android.ui.base.MvpView;

public interface FeedMvpView extends MvpView {

    void showPosts(List<Post> posts);

    void showPostProgress(boolean show);

    void showEmptyMessage();

    void showPostsError();

    void addPosts(List<Post> posts);

    void setPostLikesCount(int postId, Integer likesCount, boolean favorite);

    void scrollToTop();

    void setItemToScroll(Integer startTime);

    void scrollToLastViewed();
}
