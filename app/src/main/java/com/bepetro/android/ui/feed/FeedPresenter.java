package com.bepetro.android.ui.feed;


import com.bepetro.android.data.DataManager;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.data.remote.BePetroService;
import com.bepetro.android.ui.base.Presenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class FeedPresenter implements Presenter<FeedMvpView> {

    private final DataManager mDataManager;
    public Subscription mSubscription;
    private FeedMvpView mMvpView;
    private int mStartFrom;
    private final int DEFAULT_POSTS_COUNT = 50; // default count loading posts
    private List<Post> mPosts;
    private Integer startTime;

    @Inject
    public FeedPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(FeedMvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void loadMorePosts() {
        loadMorePosts(DEFAULT_POSTS_COUNT);
    }

    public void loadMorePosts(int count) {
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = getMorePostsObservable(mStartFrom, count)
                .subscribe(new Subscriber<List<Post>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<Post> posts) {
                        if (!posts.isEmpty()) { // todo: what if is empty?
                            mMvpView.addPosts(posts);
                        }
                    }
                });
    }

    public void loadPosts() {
        loadPosts(DEFAULT_POSTS_COUNT, null);
    }

    /**
     * Load the list of Posts
     *
     * @param count count of loading posts
     */
    public void loadPosts(final int count, Integer nextFromLocal) {

        if (mPosts != null && startTime == null) {
            startTime = mPosts.get(0).date;
        }

        mMvpView.showPostProgress(true);
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = getPostsObservable(count, nextFromLocal)
                .subscribe(new Subscriber<BePetroService.PostResponse>() {
                    @Override
                    public void onCompleted() {
                        mMvpView.showPostProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMvpView.showPostProgress(false);
                        mMvpView.showPostsError();
                    }

                    @Override
                    public void onNext(BePetroService.PostResponse response) {
                        if (response.nextFrom != null)
                            mStartFrom = response.nextFrom;
                        if (response.posts.isEmpty() && mPosts == null) { // for first start - when no posts exist (no cache no loaded)
                            mMvpView.showEmptyMessage();
                        } else {
                            if (mPosts == null) {
                                mPosts = response.posts;
                                mMvpView.showPosts(mPosts);
                            } else {
                                for (Post post : response.posts) {
                                    if (post != null)
                                        mPosts.add(0, post);
                                }
                                mMvpView.showPosts(null);

                                if (response.nextFrom != null) {
                                    loadPosts(count, response.nextFrom);
                                } else {
                                    startTime = null;
                                }
                            }
                        }
                    }
                });
    }

    private Observable<BePetroService.PostResponse> getPostsObservable(int count, Integer nextFromLocal) {
        if (startTime == null) {
            return mDataManager.getPosts(count)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io());
        } else {
            return mDataManager.getNewPosts(count, startTime, nextFromLocal)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io());
        }

    }

    private Observable<List<Post>> getMorePostsObservable(int startFrom, int count) {
        return mDataManager.getMorePosts(startFrom, count)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).map(new Func1<BePetroService.PostResponse, List<Post>>() {
                    @Override
                    public List<Post> call(BePetroService.PostResponse postResponse) {
                        mStartFrom = postResponse.nextFrom;
                        mPosts.addAll(postResponse.posts);
                        return postResponse.posts;
                    }
                });
    }

    public Integer[] getFilters() {
        return mDataManager.getPreferencesHelper().getPostFilterIndices();
    }

    public void saveFilters(Integer[] filters) {
        mDataManager.getPreferencesHelper().putPostFilter(filters);
    }

    public void setFavoritePost(final int postId) {
        mDataManager.setFavoritePost(postId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BePetroService.LikeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("setFavoritePost ERROR");
                    }

                    @Override
                    public void onNext(BePetroService.LikeResponse response) {
                        mMvpView.setPostLikesCount(postId, response.likes, true);
                    }
                });

    }

    public void deleteFavoritePost(final int postId) {
        mDataManager.deleteFavoritePost(postId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BePetroService.LikeResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("deleteFavoritePost ERROR");
                    }

                    @Override
                    public void onNext(BePetroService.LikeResponse response) {
                        mMvpView.setPostLikesCount(postId, response.likes, false);
                    }
                });
    }
}
