package com.bepetro.android.ui.favorite;

import com.bepetro.android.data.model.Post;
import com.bepetro.android.ui.base.MvpView;

import java.util.List;

public interface FavoriteMvpView extends MvpView {

    void showPosts(List<Post> posts);

    void showPostProgress(boolean show);

    void showEmptyMessage();

    void showPostsError();

    void addPosts(List<Post> posts);

    void setPostLikesCount(int postId, Integer likesCount, boolean favorite);

    void scrollToTop();
}
