package com.bepetro.android.ui.signin;

import javax.inject.Inject;

import com.bepetro.android.data.DataManager;
import com.bepetro.android.data.model.Petro;
import com.bepetro.android.ui.base.Presenter;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SignInPresenter implements Presenter<SignInMvpView> {

    private final DataManager mDataManager;
    private SignInMvpView mMvpView;
    private Subscription mSubscription;

    @Inject
    public SignInPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(SignInMvpView mvpView) {
        this.mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void signIn() {
        mMvpView.showProgress(true);
        mSubscription = mDataManager.signIn()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Petro>() {
                    @Override
                    public void onCompleted() {
                        mMvpView.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMvpView.showProgress(false); // todo: check if error
                        /*if (e instanceof UserRecoverableAuthException) {
                            Timber.w("UserRecoverableAuthException has happen");
                            Intent recover = ((UserRecoverableAuthException) e).getIntent();
                            mMvpView.onUserRecoverableAuthException(recover);
                        } else {
                            mMvpView.setSignInButtonEnabled(true);
                            if (NetworkUtil.isHttpStatusCode(e, 403)) {
                                // Google Auth was successful, but the user does not have a petro
                                // profile set up.
                                mMvpView.showProfileNotFoundError(account.postText);
                            } else {
                                mMvpView.showGeneralSignInError();
                            }
                        }*/
                    }

                    @Override
                    public void onNext(Petro petro) {
                        mMvpView.onSignInSuccessful();
                    }
                });
    }

}
