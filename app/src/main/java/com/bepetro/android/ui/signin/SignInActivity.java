package com.bepetro.android.ui.signin;

import android.accounts.AccountManager;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bepetro.android.R;
import com.bepetro.android.data.DataManager;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.ui.main.MainActivity;
import com.bepetro.android.util.DialogFactory;

public class SignInActivity extends BaseActivity implements SignInMvpView {

    private static final String EXTRA_POPUP_MESSAGE =
            "com.bepetro.android.ui.signin.SignInActivity.EXTRA_POPUP_MESSAGE";
    private static final int REQUEST_CODE_AUTH_EXCEPTION = 4;

    @Inject
    AccountManager mAccountManager;
    @Inject
    DataManager mDataManager;
    @Inject
    SignInPresenter mSignInPresenter;
    private boolean mShouldFinishOnStop; // When signIn is successful - finish this activity

    @BindView(R.id.progress)
    ProgressBar mProgressBar;
    @BindView(R.id.text_welcome)
    TextView mWelcomeTextView;


    public static Intent getStartIntent(Context context, boolean clearPreviousActivities) {
        Intent intent = new Intent(context, SignInActivity.class);
        if (clearPreviousActivities) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        return intent;
    }

    // popUpMessage will show on a Dialog as soon as the Activity opens
    public static Intent getStartIntent(Context context,
                                        boolean clearPreviousActivities,
                                        String popUpMessage) {
        Intent intent = getStartIntent(context, clearPreviousActivities);
        intent.putExtra(EXTRA_POPUP_MESSAGE, popUpMessage);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShouldFinishOnStop = false;
        setContentView(R.layout.activity_sign_in);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mSignInPresenter.attachView(this);

        String popUpMessage = getIntent().getStringExtra(EXTRA_POPUP_MESSAGE);
        if (popUpMessage != null) {
            DialogFactory.createSimpleOkErrorDialog(this, popUpMessage).show();
        }

        mSignInPresenter.signIn();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mShouldFinishOnStop) finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSignInPresenter.detachView();
    }

    /*****
     * MVP View methods implementation
     *****/

    @Override
    public void onSignInSuccessful() {
        Intent intent = MainActivity.getStartIntent(this, false);
        //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent);
        /*} else {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                    mBePetroLogo, getString(R.string.transition_bepetro_logo));
            startActivity(intent, options.toBundle());
        }*/
        // We need this flag because if we call finish() here, the activity transition won't work
        mShouldFinishOnStop = true;
    }

    @Override
    public void onUserRecoverableAuthException(Intent recoverIntent) {
        startActivityForResult(recoverIntent, REQUEST_CODE_AUTH_EXCEPTION);
    }

    @Override
    public void showProgress(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProfileNotFoundError(String accountName) {
        DialogFactory.createSimpleOkErrorDialog(this,
                getString(R.string.error_bepetro_profile_not_found)).show();
    }

    @Override
    public void showGeneralSignInError() {
        DialogFactory.createSimpleOkErrorDialog(this, getString(R.string.error_sign_in)).show();
    }


}
