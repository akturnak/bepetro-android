package com.bepetro.android.ui;

import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import com.bepetro.android.data.DataManager;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.ui.main.MainActivity;
import com.bepetro.android.ui.signin.SignInActivity;

import timber.log.Timber;

public class LauncherActivity extends BaseActivity {

    @Inject
    DataManager mDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        Intent intent;
        if (mDataManager.getPreferencesHelper().getAccessToken() != null) {
            intent = MainActivity.getStartIntent(this, false);
        } else {
            intent = SignInActivity.getStartIntent(this, false);
        }
        startActivity(intent);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        finish();
    }
}
