package com.bepetro.android.ui.signin;

import android.content.Intent;

import com.bepetro.android.ui.base.MvpView;

public interface SignInMvpView extends MvpView {

    void onSignInSuccessful();

    void onUserRecoverableAuthException(Intent recoverIntent);

    void showProgress(boolean show);

    void showProfileNotFoundError(String accountName);

    void showGeneralSignInError();

}
