package com.bepetro.android.ui.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;


public class LinearRecyclerView extends RecyclerView {
    private LinearLayoutManager mLinearLayoutManager;

    public LinearRecyclerView(Context context) {
        super(context);
        init(context, null);
    }

    public LinearRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LinearRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mLinearLayoutManager = new LinearLayoutManager(context);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(mLinearLayoutManager);
    }

    public LinearLayoutManager getLayoutManager() {
        return mLinearLayoutManager;
    }

    public int getFirstVisibleItemPosition() {
        return mLinearLayoutManager.findFirstVisibleItemPosition();
    }

    public int getFirstCompletelyVisibleItemPosition() {
        return mLinearLayoutManager.findFirstCompletelyVisibleItemPosition();
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
    }

}
