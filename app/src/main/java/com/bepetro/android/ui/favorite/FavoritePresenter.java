package com.bepetro.android.ui.favorite;


import com.bepetro.android.data.DataManager;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.data.remote.BePetroService;
import com.bepetro.android.ui.base.Presenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class FavoritePresenter implements Presenter<FavoriteMvpView> {

    private final DataManager mDataManager;
    public Subscription mSubscription;
    private FavoriteMvpView mMvpView;
    private int mLoadedPostsCount;
    private final int DEFAULT_POSTS_COUNT = 50; // default count loading posts

    @Inject
    public FavoritePresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(FavoriteMvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void loadMorePosts() {
        loadMorePosts(DEFAULT_POSTS_COUNT);
    }

    public void loadMorePosts(int count) {
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = getMorePostsObservable(mLoadedPostsCount, count)
                .subscribe(new Subscriber<List<Post>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<Post> posts) {
                        if (!posts.isEmpty()) { // todo: what if is empty?
                            mMvpView.addPosts(posts);
                        }
                    }
                });
    }

    public void loadPosts() {
        loadPosts(DEFAULT_POSTS_COUNT);
    }

    /**
     * Load the list of Posts
     *
     * @param count count of loading posts
     */
    public void loadPosts(final int count) {
        mMvpView.showPostProgress(true);
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = getPostsObservable(count)
                .subscribe(new Subscriber<BePetroService.PostResponse>() {
                    @Override
                    public void onCompleted() {
                        mMvpView.showPostProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mMvpView.showPostProgress(false);
                        mMvpView.showPostsError();
                    }

                    @Override
                    public void onNext(BePetroService.PostResponse response) {
                        mLoadedPostsCount = count;
                        if (response.posts.isEmpty()) {
                            mMvpView.showEmptyMessage();
                        } else {
                            mMvpView.showPosts(response.posts);
                        }
                    }
                });
    }

    private Observable<BePetroService.PostResponse> getPostsObservable(int count) {
        return mDataManager.getFavorites(count)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    private Observable<List<Post>> getMorePostsObservable(int offset, final int count) {
        return mDataManager.getMoreFavorites(offset, count)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).map(new Func1<BePetroService.PostResponse, List<Post>>() {
                    @Override
                    public List<Post> call(BePetroService.PostResponse postResponse) {
                        mLoadedPostsCount += count;
                        return postResponse.posts;
                    }
                });
    }

    public Integer[] getFilters() {
        return mDataManager.getPreferencesHelper().getPostFilterIndices();
    }

    public void saveFilters(Integer[] filters) { // todo: set exclusive filter for this tab
        mDataManager.getPreferencesHelper().putPostFilter(filters);
    }

    public void setFavoritePost(final int postId) {
        mDataManager.setFavoritePost(postId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BePetroService.LikeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("setFavoritePost ERROR");
                    }

                    @Override
                    public void onNext(BePetroService.LikeResponse response) {
                        mMvpView.setPostLikesCount(postId, response.likes, true);
                    }
                });

    }

    public void deleteFavoritePost(final int postId) {
        mDataManager.deleteFavoritePost(postId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BePetroService.LikeResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("deleteFavoritePost ERROR");
                    }

                    @Override
                    public void onNext(BePetroService.LikeResponse response) {
                        mMvpView.setPostLikesCount(postId, response.likes, false);
                    }
                });
    }
}
