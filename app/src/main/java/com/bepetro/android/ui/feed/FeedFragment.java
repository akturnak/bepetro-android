package com.bepetro.android.ui.feed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bepetro.android.R;
import com.bepetro.android.data.BusEvent;
import com.bepetro.android.data.model.LongPostAttachment;
import com.bepetro.android.data.model.PhotoAttachment;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.injection.ActivityContext;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.ui.widget.EndlessRecyclerViewScrollListener;
import com.bepetro.android.ui.widget.LinearRecyclerView;
import com.bepetro.android.util.DialogFactory;
import com.bepetro.android.util.NetworkUtil;
import com.bepetro.android.util.ViewUtil;
import com.squareup.otto.Bus;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class FeedFragment extends Fragment implements FeedMvpView, FeedAdapter.OnItemClickListener {

    @Inject
    FeedPresenter mFeedPresenter;
    @Inject
    FeedAdapter mFeedAdapter;
    @Inject
    @ActivityContext
    Context mContext;
    @Inject
    Bus eventBus;

    @BindView(R.id.recycler_view_post)
    LinearRecyclerView mPostRecycler;
    @BindView(R.id.swipe_refresh_container)
    SwipeRefreshLayout mSwipeRefreshContainer;
    @BindView(R.id.text_no_posts)
    TextView mNoPostsText;
    @BindView(R.id.progress)
    ProgressBar mProgress;

    private Menu mOptionsMenu;
    public boolean haveNewPosts = false;
    private Integer mPostDateForScroll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, fragmentView);
        mFeedPresenter.attachView(this);
        // Performance optimization
        mPostRecycler.setHasFixedSize(true);

        mFeedAdapter.setOnItemClickListener(this); // todo: deattach from adapter to prevent memory liking
        mPostRecycler.setAdapter(mFeedAdapter);
        mPostRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(mPostRecycler.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                mFeedPresenter.loadMorePosts(); // TODO: 8/15/16 may be fetch items here?
                mFeedAdapter.showLoading(true);
            }
        });

        mPostRecycler.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshContainer.setColorSchemeResources(R.color.primary);  // TODO: why not in styles?
        mSwipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPosts();
            }
        });

        mFeedPresenter.loadPosts();

        return fragmentView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mFeedPresenter.detachView();
    }

    @Override
    public void showPosts(List<Post> posts) {
        if (posts != null) {
            mFeedAdapter.setPosts(posts);
        }
        mFeedAdapter.notifyDataSetChanged();
        mNoPostsText.setVisibility(View.GONE);
        haveNewPosts = false;
        eventBus.post(new BusEvent.StartNewPostsChecking());
    }


    @Override
    public void showPostProgress(boolean show) {
        mSwipeRefreshContainer.setRefreshing(show);
        if (show && mFeedAdapter.getItemCount() == 0) {
            mProgress.setVisibility(View.VISIBLE);
        } else {
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyMessage() {
        mNoPostsText.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPostsError() {
        DialogFactory.createSimpleOkErrorDialog(getActivity(),
                getString(R.string.error_loading_posts)).show();
    }

    @Override
    public void addPosts(List<Post> posts) {
        Timber.d("FavoriteFragment.addPosts!!!");
        mFeedAdapter.showLoading(false);
        //mFeedAdapter.addPosts(posts);
        mFeedAdapter.notifyDataSetChanged();
        //this.mOptionsMenu.getItem(0).setVisible(true);
    }

    @Override
    public void setPostLikesCount(int postId, Integer likesCount, boolean favorite) {
        // why not just save position when call addLike?
        // because data can be loaded in background and position of post can be changed
        int position;
        position = mFeedAdapter.setPostLikesCount(postId, likesCount, favorite);
        mFeedAdapter.notifyItemChanged(position);
    }

    @Override
    public void scrollToTop() {
        if (mPostRecycler != null) {
            int visibleItemPosition = mPostRecycler.getFirstCompletelyVisibleItemPosition();
            if (visibleItemPosition == RecyclerView.NO_POSITION)
                visibleItemPosition = mPostRecycler.getFirstVisibleItemPosition();
            mPostRecycler.scrollToPosition(0); // scroll to first post
            if (haveNewPosts) {
                eventBus.post(new BusEvent.ViewPostsUpdates());
                mPostDateForScroll = mFeedAdapter.getItemDate(visibleItemPosition);
                mFeedAdapter.setCheckingPostDate(mPostDateForScroll);
                loadPosts();
            }
        }
    }

    @Override
    public void setItemToScroll(Integer startTime) {
    }

    @Override
    public void scrollToLastViewed() {
        if (mPostDateForScroll != null) {
            eventBus.post(new BusEvent.NewPostsViewed());
            mPostRecycler.scrollToPosition(mFeedAdapter.getPostPositionByDate(mPostDateForScroll));
        }
    }

    private void loadPosts() {
        mFeedPresenter.loadPosts(); // todo: does need load posts again if the already exist?
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
        this.mOptionsMenu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                //startActivity(new Intent(this, CheckInActivity.class));
                DialogFactory.createSimpleOkErrorJokeDialog(getActivity(),
                        "Простите", "Настройки будут чуть-чуть позже").show();
                return true;
            case R.id.action_posts_filter:
                Integer[] savedFilters = mFeedPresenter.getFilters();
                MaterialDialog filtersDialog = new MaterialDialog.Builder(getActivity())
                        .title(R.string.filters_dialog_title)
                        .items(R.array.posts_filters)
                        .itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                                if (which.length <= 0) { // if no filters selected - disable saving
                                    dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                                } else {
                                    dialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                                }
                                return true;
                            }
                        })
                        .alwaysCallMultiChoiceCallback() // the callback will always be called, to check what at least one choice selected
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                mFeedPresenter.saveFilters(dialog.getSelectedIndices());
                            }
                        })
                        .positiveText(R.string.filters_dialog_positive)
                        .build();
                filtersDialog.setSelectedIndices(savedFilters);
                filtersDialog.show();

                return true;
            case R.id.action_new:
                this.mOptionsMenu.getItem(0).setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(Post post, ACTION action, RecyclerView.ViewHolder holder) {
        boolean connected = NetworkUtil.isNetworkConnected(mContext); // is network available?
        switch (action) {
            case LAST_VIEWED_REACHED:
                eventBus.post(new BusEvent.NewPostsViewed());
                break;

            case COMPLAIN:

                break;
            case SOURCE:

                break;
            case SET_FAVORITE:
                if (connected) {
                    setPostLikesCount(post.id, post.likes++, true);
                    mFeedPresenter.setFavoritePost(post.id);
                } else {

                }
                break;
            case DELETE_FAVORITE:
                if (connected) {
                    setPostLikesCount(post.id, post.likes--, false);
                    mFeedPresenter.deleteFavoritePost(post.id);
                } else {

                }
                break;
            case SHARE:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                if (post.attachments.size() == 0 || post.attachments.get(0) instanceof LongPostAttachment) {
                    sendIntent.putExtra(Intent.EXTRA_TEXT, post.text);
                    sendIntent.setType("text/plain");
                } else if (post.attachments.get(0) instanceof PhotoAttachment) {
                    Uri bmpUri = ViewUtil.getLocalBitmapUri(((FeedAdapter.PhotoHolder) holder).postPhoto, mContext);
                    if (bmpUri != null) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        sendIntent.setType("image/jpeg");
                    } else {
                        // .. sharing failed, handle error
                    }
                }
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                break;
            case COMMENT:
                DialogFactory.createSimpleOkErrorJokeDialog(getActivity(),
                        "Извини", "Комментарии будут чуть-чуть позже").show();
                break;
            case OPEN_POST_FULLSCREEN:
                ActivityPostDetails.navigate((AppCompatActivity) getActivity(), ((FeedAdapter.PhotoHolder) holder).postPhoto, post, 111);
                break;
        }

    }
}