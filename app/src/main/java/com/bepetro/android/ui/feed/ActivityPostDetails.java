package com.bepetro.android.ui.feed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bepetro.android.R;
import com.bepetro.android.data.model.LongPostAttachment;
import com.bepetro.android.data.model.PhotoAttachment;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.injection.ActivityContext;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.util.NetworkUtil;
import com.bepetro.android.util.ViewUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Activity for detail post view (fullscreen image)
 */
public class ActivityPostDetails extends BaseActivity implements FeedMvpView {

    public static String KEY_POST = "com.bepetro.android.data.model.Post";

    private View parent_view;

    private Post mPost;

    PhotoViewAttacher mAttacher;

    @Inject
    FeedPresenter mFeedPresenter;

    @Inject
    @ActivityContext
    Context mContext;

    @BindView(R.id.post_detail_toolbar)
    public Toolbar toolbar;

    @BindView(R.id.post_detail_bottom_menu)
    public LinearLayout postBottomMenu;

    @BindView(R.id.iv_post_likes)
    public ImageView ivLike;

    @BindView(R.id.tv_post_likes)
    public TextView tvLikesCount;

    @BindView(R.id.post_comments)
    public TextView tvCommentsCount;

    @BindView(R.id.likesWrapper)
    public LinearLayout likesWrapper;

    @BindView(R.id.shareWrapper)
    public LinearLayout shareWrapper;

    @BindView((R.id.post_detail_image))
    public ImageView ivPostImage;

    private boolean hideBars = false; // flag for Image tap - bars already hidden or not

    private boolean isChanged = false; // set to true if post information changed (added/removed from favorites, added comment)

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Post post, int requestCode) {
        Intent intent = new Intent(activity, ActivityPostDetails.class);
        intent.putExtra(KEY_POST, post);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, KEY_POST);
        ActivityCompat.startActivityForResult(activity, intent, requestCode, options.toBundle());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_post_details);
        ButterKnife.bind(this);

        mFeedPresenter.attachView(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        //
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(parent_view, KEY_POST);

        // initialize conversation data
        Intent intent = getIntent();
        mPost = (Post) intent.getExtras().getSerializable(KEY_POST);

        String url = ((PhotoAttachment) mPost.attachments.get(0)).srcBig;

        ImageView postImage = (ImageView) findViewById(R.id.post_detail_image);

        Picasso.with(this).load(url).into(postImage);

        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
        // (not needed unless you are going to change the drawable later)
        mAttacher = new PhotoViewAttacher(postImage);
        mAttacher.setOnPhotoTapListener(new PhotoTapListener());

        setUpBottomMenu();
    }

    @Override
    protected void onDestroy() {
        mFeedPresenter.detachView();
        setActivityResult();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setActivityResult();
        super.onBackPressed();
    }

    private void setActivityResult() {
        if (isChanged) {
            Bundle postData = new Bundle();
            postData.putSerializable(KEY_POST, mPost);
            Intent intent = new Intent();
            intent.putExtras(postData);
            setResult(RESULT_OK, intent);
        }
    }

    @OnClick(R.id.likesWrapper)
    public void like() {
        if (connected()) {
            isChanged = true;
            if (mPost.favorite) {
                // delete from favorite
                setPostLikesCount(mPost.id, mPost.likes--, false);
                mFeedPresenter.deleteFavoritePost(mPost.id);
            } else {
                // add to favorite
                setPostLikesCount(mPost.id, mPost.likes++, true);
                mFeedPresenter.setFavoritePost(mPost.id);
            }
        }
    }

    @OnClick(R.id.shareWrapper)
    public void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (mPost.attachments.size() == 0 || mPost.attachments.get(0) instanceof LongPostAttachment) {
            sendIntent.putExtra(Intent.EXTRA_TEXT, mPost.text);
            sendIntent.setType("text/plain");
        } else if (mPost.attachments.get(0) instanceof PhotoAttachment) {
            Uri bmpUri = ViewUtil.getLocalBitmapUri(ivPostImage, mContext);
            if (bmpUri != null) {
                sendIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                sendIntent.setType("image/jpeg");
            } else {
                // .. sharing failed, handle error
            }
        }
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    private boolean connected() {
        // is network available?
        return NetworkUtil.isNetworkConnected(mContext);
    }

    private void setUpBottomMenu() {
        if (mPost.favorite) {
            ivLike.setImageResource(R.drawable.ic_heart_fav);
        } else {
            ivLike.setImageResource(R.drawable.ic_heart);
        }

        if (mPost.likes > 0) {
            tvLikesCount.setText("" + mPost.likes);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_feed_popup, menu);
        return true;
    }

    @Override
    public void showPosts(List<Post> posts) {

    }

    @Override
    public void showPostProgress(boolean show) {

    }

    @Override
    public void showEmptyMessage() {

    }

    @Override
    public void showPostsError() {

    }

    @Override
    public void addPosts(List<Post> posts) {

    }

    @Override
    public void setPostLikesCount(int postId, Integer likesCount, boolean favorite) {
        mPost.favorite = favorite;
        mPost.likes = likesCount;
        setUpBottomMenu();
    }

    @Override
    public void scrollToTop() {

    }

    @Override
    public void setItemToScroll(Integer startTime) {

    }

    @Override
    public void scrollToLastViewed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class PhotoTapListener implements PhotoViewAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
            hideBars();
        }

        @Override
        public void onOutsidePhotoTap() {
            hideBars();
        }
    }

    private void hideBars() {
        if (hideBars) {
            toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
            postBottomMenu.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
            hideBars = !hideBars;
        } else {
            toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
            postBottomMenu.animate().translationY(+postBottomMenu.getTop()).setInterpolator(new AccelerateInterpolator()).start();
            hideBars = !hideBars;
        }
    }

}