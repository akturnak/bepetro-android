package com.bepetro.android.ui.favorite;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bepetro.android.R;
import com.bepetro.android.data.model.LongPostAttachment;
import com.bepetro.android.data.model.PhotoAttachment;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.injection.ActivityContext;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.ui.widget.EndlessRecyclerViewScrollListener;
import com.bepetro.android.ui.widget.LinearRecyclerView;
import com.bepetro.android.util.DialogFactory;
import com.bepetro.android.util.NetworkUtil;
import com.bepetro.android.util.ViewUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class FavoriteFragment extends Fragment implements FavoriteMvpView, FavoriteAdapter.OnItemClickListener {

    @Inject
    FavoritePresenter mFavoritePresenter;
    @Inject
    FavoriteAdapter mFavoriteAdapter;
    @Inject
    @ActivityContext
    Context mContext;

    @BindView(R.id.recycler_view_post)
    LinearRecyclerView mPostRecycler;
    @BindView(R.id.swipe_refresh_container)
    SwipeRefreshLayout mSwipeRefreshContainer;
    @BindView(R.id.text_no_posts)
    TextView mNoPostsText;
    @BindView(R.id.progress)
    ProgressBar mProgress;

    private Menu mOptionsMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Timber.d("FavoriteFragment.onCreate!!!");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        ((BaseActivity) getActivity()).activityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        Timber.d("FavoriteFragment.onCreateView!!!");
        View fragmentView = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, fragmentView);
        mFavoritePresenter.attachView(this);
        // Performance optimization
        mPostRecycler.setHasFixedSize(true);

        mFavoriteAdapter.setOnItemClickListener(this); // todo: deattach from adapter to prevent memory liking
        mPostRecycler.setAdapter(mFavoriteAdapter);
        mPostRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(mPostRecycler.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                mFavoritePresenter.loadMorePosts(); // TODO: 8/15/16 may be fetch items here?
                mFavoriteAdapter.showLoading(true);
            }
        });

        mPostRecycler.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshContainer.setColorSchemeResources(R.color.primary);  // TODO: why not in styles?
        mSwipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFavoritePresenter.loadPosts(); // todo: does need load posts again if the already exist?
            }
        });

        mFavoritePresenter.loadPosts();

        return fragmentView;
    }

    @Override
    public void onStart() {
        Timber.d("FavoriteFragment.OnStart!!!");
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        Timber.d("FavoriteFragment.onDestroyView!!!");
        super.onDestroyView();
        mFavoritePresenter.detachView();
    }

    @Override
    public void showPosts(List<Post> posts) {
        Timber.d("FavoriteFragment.showPosts!!!");
        mFavoriteAdapter.setPosts(posts);
        mFavoriteAdapter.notifyDataSetChanged();
        mNoPostsText.setVisibility(View.GONE);
    }


    @Override
    public void showPostProgress(boolean show) {
        Timber.d("FavoriteFragment.showPostProgress!!!");
        mSwipeRefreshContainer.setRefreshing(show);
        if (show && mFavoriteAdapter.getItemCount() == 0) {
            mProgress.setVisibility(View.VISIBLE);
        } else {
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyMessage() {
        mNoPostsText.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPostsError() {
        DialogFactory.createSimpleOkErrorDialog(getActivity(),
                getString(R.string.error_loading_posts)).show();
    }

    @Override
    public void addPosts(List<Post> posts) {
        Timber.d("FavoriteFragment.addPosts!!!");
        mFavoriteAdapter.showLoading(false);
        mFavoriteAdapter.addPosts(posts);
        mFavoriteAdapter.notifyDataSetChanged();
        //this.mOptionsMenu.getItem(0).setVisible(true);
    }

    @Override
    public void setPostLikesCount(int postId, Integer likesCount, boolean favorite) {
        mFavoriteAdapter.removePost(postId);
    }

    @Override
    public void scrollToTop() {
        if (mPostRecycler != null)
            mPostRecycler.scrollToPosition(0); // scroll to first post
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
        this.mOptionsMenu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                //startActivity(new Intent(this, CheckInActivity.class));
                DialogFactory.createSimpleOkErrorJokeDialog(getActivity(),
                        "Простите", "Настройки будут чуть-чуть позже").show();
                return true;
            case R.id.action_posts_filter:
                Integer[] savedFilters = mFavoritePresenter.getFilters();
                MaterialDialog filtersDialog = new MaterialDialog.Builder(getActivity())
                        .title(R.string.filters_dialog_title)
                        .items(R.array.posts_filters)
                        .itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                                if (which.length <= 0) { // if no filters selected - disable saving
                                    dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                                } else {
                                    dialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                                }
                                return true;
                            }
                        })
                        .alwaysCallMultiChoiceCallback() // the callback will always be called, to check what at least one choice selected
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                mFavoritePresenter.saveFilters(dialog.getSelectedIndices());
                            }
                        })
                        .positiveText(R.string.filters_dialog_positive)
                        .build();
                filtersDialog.setSelectedIndices(savedFilters);
                filtersDialog.show();

                return true;
            case R.id.action_new:
                this.mOptionsMenu.getItem(0).setVisible(false);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(Post post, ACTION action, RecyclerView.ViewHolder holder) {
        boolean connected = NetworkUtil.isNetworkConnected(mContext); // is network available?
        switch (action) {
            case COMPLAIN:

                break;
            case SOURCE:

                break;
            case SET_FAVORITE:
                if (connected) {
                    setPostLikesCount(post.id, post.likes++, true);
                    mFavoritePresenter.setFavoritePost(post.id);
                } else {

                }
                break;
            case DELETE_FAVORITE:
                if (connected) {
                    setPostLikesCount(post.id, post.likes--, false);
                    mFavoritePresenter.deleteFavoritePost(post.id);
                } else {

                }
                break;
            case SHARE:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                if (post.attachments.size() == 0 || post.attachments.get(0) instanceof LongPostAttachment) {
                    sendIntent.putExtra(Intent.EXTRA_TEXT, post.text);
                    sendIntent.setType("text/plain");
                } else if (post.attachments.get(0) instanceof PhotoAttachment) {
                    Uri bmpUri = ViewUtil.getLocalBitmapUri(((FavoriteAdapter.PhotoHolder) holder).postPhoto, mContext);
                    if (bmpUri != null) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        sendIntent.setType("image/jpeg");
                    } else {
                        // .. sharing failed, handle error
                    }
                }
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                break;
            case COMMENT:
                DialogFactory.createSimpleOkErrorJokeDialog(getActivity(),
                        "Извини", "Комментарии будут чуть-чуть позже").show();
                break;
            case OPEN_POST_FULLSCREEN:
                ActivityPostDetails.navigate((AppCompatActivity) getActivity(), ((FavoriteAdapter.PhotoHolder) holder).postPhoto, post, 111);
                break;
        }

    }
}