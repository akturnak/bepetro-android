package com.bepetro.android.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bepetro.android.R;
import com.bepetro.android.data.BusEvent;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.service.PostsSyncService;
import com.bepetro.android.ui.base.BaseActivity;
import com.bepetro.android.ui.favorite.FavoriteFragment;
import com.bepetro.android.ui.favorite.FavoriteMvpView;
import com.bepetro.android.ui.feed.ActivityPostDetails;
import com.bepetro.android.ui.feed.FeedFragment;
import com.bepetro.android.ui.feed.FeedMvpView;
import com.bepetro.android.ui.signin.SignInActivity;
import com.bepetro.android.ui.widget.TwoFragment;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity implements MainMvpView {

    private static final String EXTRA_AUTO_CHECK_IN_DISABLED =
            "com.bepetro.android.ui.main.MainActivity.EXTRA_AUTO_CHECK_IN_DISABLED";

    @Inject
    MainPresenter mMainPresenter;

    @Inject
    Bus mEventBus;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FloatingActionButton fab;
    private int[] tabIcons = {
            R.drawable.ic_dashboard_white_24dp,
            R.drawable.ic_track_changes_white_24dp,
            R.drawable.ic_favorite_white_24dp
    };

    private FeedMvpView feedMvpView;
    private FavoriteMvpView favoriteMvpView;
    private ViewPagerAdapter adapter;
    private Handler handler;
    private Runnable runnableCode;

    /**
     * Create an Intent for the main activity.
     * Set autoCheckInDisabled to true if you want to prevent this activity from
     * triggering any auto check-in related service on create.
     * This is useful for testing purposes.
     */
    public static Intent getStartIntent(Context context, boolean autoCheckInDisabled) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_AUTO_CHECK_IN_DISABLED, autoCheckInDisabled);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.hide();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feedMvpView != null)
                    feedMvpView.scrollToLastViewed();
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() != 0) {
                    fab.hide();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // when "home" tab selected and pressed again - scroll to top of the list
                if (tab.getPosition() == 0) {
                    feedMvpView.scrollToTop();
                } else if (tab.getPosition() == 2) {
                    favoriteMvpView.scrollToTop();
                }
            }
        });


        mMainPresenter.attachView(this);

        mEventBus.register(this);

        // Trigger auto check-in related services, unless the flag indicates not to.
        boolean autoCheckInDisabled = getIntent()
                .getBooleanExtra(EXTRA_AUTO_CHECK_IN_DISABLED, false);
        if (!autoCheckInDisabled && savedInstanceState == null) {
            startService(PostsSyncService.getStartIntent(this));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startCheckingNewPosts() { // todo: set filter when checking new posts
        stopCheckingNewPosts(); // todo: think - do we need this?
        // Create the Handler object
        handler = new Handler();
        // Define the code block to be executed
        runnableCode = new Runnable() {
            @Override
            public void run() {
                Timber.d("start new post checking");
                // Check new posts
                mMainPresenter.checkNewPosts();
                // Repeat this the same runnable code block again another 5 minute
                handler.postDelayed(runnableCode, 300000);
            }
        };
        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);
    }

    private void stopCheckingNewPosts() {
        // remove pending code execution
        if (handler != null && runnableCode != null) {
            handler.removeCallbacks(runnableCode);
        }
    }


    @Override
    protected void onPause() {
        stopCheckingNewPosts();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mMainPresenter.detachView();
        mEventBus.unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check request is from ActivityPostDetail
        if (requestCode == 111) {
            // Make sure the data is changed
            if (resultCode == RESULT_OK) {
                Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
                if (fragment instanceof FeedFragment) {
                    // get post with updated info
                    Post post = (Post) data.getSerializableExtra(ActivityPostDetails.KEY_POST);
                    // update post info in list
                    ((FeedFragment) fragment).setPostLikesCount(post.id, post.likes, post.favorite);
                }
            }
        }
    }

    /*****
     * MVP View methods implementation
     *****/

    @Override
    public void onSignedOut() {
        startActivity(SignInActivity.getStartIntent(this, true));
    }

    @Override
    public void haveNewPosts() {
        stopCheckingNewPosts();
        setupTabIconForNewPost();
        if (feedMvpView != null) {
            ((FeedFragment) feedMvpView).haveNewPosts = true;
        }
    }

    /*****
     * Tabs and ViewPager setup
     *****/

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupTabIconForNewPost() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_fiber_new_white_48dp);
    }

    private void setupViewPager(ViewPager viewPager) {
        FeedFragment feedFragment = new FeedFragment();
        FavoriteFragment favoriteFragment = new FavoriteFragment();
        favoriteMvpView = favoriteFragment;
        feedMvpView = feedFragment;
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(feedFragment, "Feed");
        adapter.addFragment(new TwoFragment(), "Select");
        adapter.addFragment(favoriteFragment, "Favorite");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Subscribe
    public void onStartNewPostsChecking(BusEvent.StartNewPostsChecking event) {
        Timber.d("!!! onStartNewPostsChecking");
        startCheckingNewPosts();
        setupTabIcons();
    }

    @Subscribe
    public void onViewPostsUpdates(BusEvent.ViewPostsUpdates event) {
        fab.show();
    }

    @Subscribe
    public void onNewPostsViewed(BusEvent.NewPostsViewed event) {
        fab.hide();
    }

}