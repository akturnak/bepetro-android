package com.bepetro.android.ui.main;

import com.bepetro.android.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void onSignedOut();

    void haveNewPosts();

}
