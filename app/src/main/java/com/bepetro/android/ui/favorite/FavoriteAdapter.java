package com.bepetro.android.ui.favorite;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bepetro.android.R;
import com.bepetro.android.data.model.LongPostAttachment;
import com.bepetro.android.data.model.PhotoAttachment;
import com.bepetro.android.data.model.Post;
import com.bepetro.android.injection.ActivityContext;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Post> mPosts;
    private OnItemClickListener mListener;

    private final int VIEW_TYPE_LOADING = 0;
    private final int VIEW_TYPE_POST = 1;
    private final int VIEW_TYPE_LONG_POST = 2;
    private final int VIEW_TYPE_PHOTO = 3;

    @Inject
    @ActivityContext
    Context mContext;


    public interface OnItemClickListener {
        enum ACTION {SET_FAVORITE, DELETE_FAVORITE, SHARE, COMPLAIN, OPEN_POST_FULLSCREEN, SOURCE, COMMENT}

        void onItemClick(Post post, ACTION action, RecyclerView.ViewHolder holder);
    }

    @Inject
    public FavoriteAdapter() {
        this.mPosts = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mListener = onItemClickListener;
    }

    public void removePost(int postId) { // todo: do it with animation
        Iterator<Post> i = mPosts.iterator();
        while (i.hasNext()) {
            Post post = i.next();
            if (post.id == postId)
                i.remove();
        }
        this.notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_feed, parent, false);
        switch (viewType) {
            case VIEW_TYPE_POST:
                return new PostHolder(view);
            case VIEW_TYPE_PHOTO:
                return new PhotoHolder(view);
            case VIEW_TYPE_LONG_POST:
                return new LongPostHolder(view);
            case VIEW_TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
                return new LoadingViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Post post = mPosts.get(position);

        if (holder instanceof BasePostHolder) {
            BasePostHolder basePostHolder = (BasePostHolder) holder;
            basePostHolder.init(post);
        } else {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    private Post getItem(int position) {
        return mPosts.get(position);
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    @Override
    public int getItemViewType(int position) { // todo: need refactor - it's ugly!
        if (mPosts.get(position) == null)
            return VIEW_TYPE_LOADING;
        if (mPosts.get(position).attachments.size() == 0)
            return VIEW_TYPE_POST;
        if (mPosts.get(position).attachments.get(0) instanceof LongPostAttachment) {
            return VIEW_TYPE_LONG_POST;
        } else if (mPosts.get(position).attachments.get(0) instanceof PhotoAttachment) {
            return VIEW_TYPE_PHOTO;
        }
        return 777;
    }

    public void setPosts(List<Post> list) {
        mPosts = list;
    }

    public void addPosts(List<Post> posts) {
        mPosts.addAll(posts);
    }

    class BasePostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.post_date)
        public TextView postDate;

        @BindView(R.id.post_likes)
        public TextView likesCount;

        @BindView(R.id.likesWrapper)
        public LinearLayout likeWrapper;

        @BindView(R.id.commentsWrapper)
        public LinearLayout commentsWrapper;

        @BindView(R.id.shareWrapper)
        public LinearLayout shareWrapper;

        @BindView(R.id.bt_more)
        public ImageView btMore;

        @BindView(R.id.bt_like)
        public ImageView btLike;

        @BindView(R.id.post_content_wrapper)
        public LinearLayout postContent;

        @BindView(R.id.long_post_content_wrapper)
        public LinearLayout longPostContent;

        @BindView(R.id.item_post_photo_wrapper)
        public LinearLayout photoContent;

        public BasePostHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            likeWrapper.setOnClickListener(this);
            commentsWrapper.setOnClickListener(this);
            shareWrapper.setOnClickListener(this);
            btMore.setOnClickListener(this);
            photoContent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) {
                return; // prevent return NO_POSITION (-1) - bcs error can occur
                // read http://stackoverflow.com/questions/29684154/recyclerview-viewholder-getlayoutposition-vs-getadapterposition
            }
            Post post = getItem(position);
            switch (v.getId()) {
                case R.id.likesWrapper:
                    if (post.favorite) {
                        mListener.onItemClick(post, OnItemClickListener.ACTION.DELETE_FAVORITE, this);
                    } else {
                        mListener.onItemClick(post, OnItemClickListener.ACTION.SET_FAVORITE, this);
                    }
                    break;
                case R.id.shareWrapper:
                    mListener.onItemClick(post, OnItemClickListener.ACTION.SHARE, this);
                    break;
                case R.id.bt_more:
                    PopupMenu popupMenu = new PopupMenu(mContext, btMore);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            return true;
                        }
                    });
                    popupMenu.inflate(R.menu.menu_feed_popup);
                    popupMenu.show();
                    break;
                case R.id.commentsWrapper:
                    mListener.onItemClick(post, OnItemClickListener.ACTION.COMMENT, this);
                    break;
                case R.id.item_post_photo_wrapper:
                    mListener.onItemClick(post, OnItemClickListener.ACTION.OPEN_POST_FULLSCREEN, this);
                    break;
            }

        }

        public void showPhotoContent() {
            postContent.setVisibility(View.GONE);
            longPostContent.setVisibility(View.GONE);
            photoContent.setVisibility(View.VISIBLE);
        }

        public void showPostContent() {
            longPostContent.setVisibility(View.GONE);
            photoContent.setVisibility(View.GONE);
            postContent.setVisibility(View.VISIBLE);
        }

        public void showLongPostContent() {
            photoContent.setVisibility(View.GONE);
            postContent.setVisibility(View.GONE);
            longPostContent.setVisibility(View.VISIBLE);
        }

        public void init(Post post) {
            Date date = new Date(post.date * 1000L); // *1000 is to convert seconds to milliseconds
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm"); // the format of your date
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            sdf.setTimeZone(tz); // give a timezone reference for formating (see comment at the bottom
            String formattedDate = sdf.format(date);
            postDate.setText(formattedDate);
            // set like icon
            if (post.favorite) {
                btLike.setImageResource(R.drawable.ic_heart_fav);
            } else {
                btLike.setImageResource(R.drawable.ic_heart);
            }
            // set post likes count
            String likes = "";
            if (post.likes > 0) {
                likes = String.valueOf(post.likes);
            }
            likesCount.setText(likes);
            // set post comments count
        }
    }

    class PostHolder extends BasePostHolder {

        @BindView(R.id.post_text)
        public TextView postText;


        public PostHolder(View itemView) {
            super(itemView);
        }

        public void init(Post post) { // todo: clean up previous data
            super.init(post);
            postText.setText(post.text);
            showPostContent();
        }
    }

    class PhotoHolder extends BasePostHolder {

        @BindView(R.id.post_photo)
        public ImageView postPhoto;

        public PhotoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void init(Post post) {
            super.init(post);
            String url = ((PhotoAttachment) post.attachments.get(0)).srcBig;
            Picasso.with(mContext).load(url).into(postPhoto);
            showPhotoContent();
        }
    }

    class LongPostHolder extends BasePostHolder {

        @BindView(R.id.long_post_text)
        public TextView longPostText;

        public LongPostHolder(View itemView) {
            super(itemView);
        }

        public void init(Post post) {
            super.init(post);
            String text = ((LongPostAttachment) post.attachments.get(0)).text;
            longPostText.setText(text);
            showLongPostContent();
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.load_item_progress_bar); // todo: change to onBind!
        }

    }

    /**
     * Set progress bar at the bottom when load data
     */

    public void showLoading(boolean show) {
        if (show) {
            if (mPosts.get(mPosts.size() - 1) == null)
                return; // prevent add several progress indicators
            mPosts.add(null);
            this.notifyItemInserted(mPosts.size() - 1);
        } else {
            mPosts.remove(mPosts.size() - 1);
            this.notifyItemRemoved(mPosts.size());
        }
    }

}