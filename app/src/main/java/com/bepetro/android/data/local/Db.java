package com.bepetro.android.data.local;

public class Db {

    public Db() {
    }

    public static final class PostTable {
        public static final String TABLE_NAME = "post";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TEXT = "text";

        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " TEXT PRIMARY KEY," +
                        COLUMN_TEXT + " TEXT," +
                        " );";

    }

}
