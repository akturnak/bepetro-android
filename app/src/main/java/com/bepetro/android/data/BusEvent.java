package com.bepetro.android.data;

public class BusEvent {

    public static class AuthenticationError {
    }

    public static class StartNewPostsChecking {
    }

    public static class ViewPostsUpdates {
    }

    public static class NewPostsViewed {
    }

    public static class UserSignedOut {
    }
}
