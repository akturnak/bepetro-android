package com.bepetro.android.data.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Deserializer for attachment filed of post.
 */
public class AttachmentDeserializer implements JsonDeserializer<Attachment> {
    @Override
    public Attachment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        String attachmentType = jsonObject.get("type").getAsString();

        JsonObject items;

        switch (attachmentType) {

            case "photo":
                PhotoAttachment photoAttachment = new PhotoAttachment();
                items =  jsonObject.get("items").getAsJsonObject();
                photoAttachment.srcBig = items.get("src_big").getAsString();
                photoAttachment.srcXBig = items.get("src_xbig").getAsString();
                photoAttachment.srcXXBig = items.get("src_xxbig").getAsString();
                return photoAttachment;

            case "long_post":
                LongPostAttachment longPostAttachment = new LongPostAttachment();
                items =  jsonObject.get("items").getAsJsonObject();
                longPostAttachment.text = items.get("text").getAsString();
                return longPostAttachment;

            default:

                return null;
        }

    }
}
