package com.bepetro.android.data.remote;

import android.content.Context;
import android.support.annotation.Nullable;

import com.bepetro.android.BuildConfig;
import com.bepetro.android.data.model.Attachment;
import com.bepetro.android.data.model.AttachmentDeserializer;
import com.bepetro.android.data.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface BePetroService {

    String ENDPOINT = "http://bepetro.com/api/v1/";
    String AUTH_HEADER = "Authorization";

    @GET("auth/access_token")
        // todo: change to POST method
    Observable<SignInResponse> signIn(@Query("client_id") String client_id,
                                      @Query("secret") String secret);

    @GET("feed")
    Observable<PostResponse> getPosts(@Header(AUTH_HEADER) String authorization,
                                      @Query("count") int count,
                                      @Query("filters") String filter);

    @GET("feed")
    Observable<PostResponse> getPosts(@Header(AUTH_HEADER) String authorization,
                                      @Query("count") int count,
                                      @Query("filters") String filter,
                                      @Query("start_from") int startFrom);

    @GET("feed")
    Observable<PostResponse> getPosts(@Header(AUTH_HEADER) String authorization,
                                      @Query("count") int count,
                                      @Query("filters") String filter,
                                      @Query("start_from") Integer startFrom,
                                      @Query("start_time") int startTime);

    @GET("users/favorites")
    Observable<PostResponse> getFavorites(@Header(AUTH_HEADER) String authorization,
                                          @Query("count") int count,
                                          @Query("filters") String filter);

    @GET("users/favorites")
    Observable<PostResponse> getMoreFavorites(@Header(AUTH_HEADER) String authorization,
                                              @Query("count") int count,
                                              @Query("filters") String filter,
                                              @Query("offset") int offset);

    @POST("posts/{postId}/likes/add")
    Observable<LikeResponse> setFavoritePost(@Header(AUTH_HEADER) String authorization,
                                             @Path("postId") Integer postId);

    @DELETE("posts/{postId}/likes/delete")
    Observable<LikeResponse> deleteFavoritePost(@Header(AUTH_HEADER) String authorization,
                                                @Path("postId") Integer postId);


    @GET("posts/last/date")
    Observable<LastPostDateResponse> getLastPostDate(@Header(AUTH_HEADER) String authorization,
                                                     @Query("filters") String filter);

    /********
     * Factory class that sets up a new bepetro services
     *******/
    class Factory {

        public static BePetroService makeBePetroService(Context context) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new UnauthorisedInterceptor(context))
                    .addInterceptor(logging)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Attachment.class, new AttachmentDeserializer())
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BePetroService.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(BePetroService.class);
        }

    }

    class Util {
        // Build API authorization string from a given access token.
        public static String buildAuthorization(String accessToken) {
            return "Token " + accessToken;
        }
    }

    /********
     * Specific request and response models
     ********/
    class SignInRequest {
        public String client_id = "rt56j90t"; // todo: convert to constant
        public String secret;

        public SignInRequest(String secret) {
            this.secret = secret;
        }
    }

    class SignInResponse {
        @SerializedName("access_token")
        public String accessToken;
    }

    class PostResponse {
        @Nullable
        @SerializedName("next_from")
        public Integer nextFrom;
        @SerializedName("items")
        public List<Post> posts;
    }

    class LikeResponse {
        @SerializedName("likes")
        public int likes;
    }

    class LastPostDateResponse {
        @SerializedName("date")
        public int date;
    }

    class UpdateCheckInRequest {
        public boolean isCheckedOut;

        public UpdateCheckInRequest(boolean isCheckedOut) {
            this.isCheckedOut = isCheckedOut;
        }
    }

}
