package com.bepetro.android.data.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Post implements Serializable {

    @SerializedName("post_type")
    public String type;
    @SerializedName("source_id")
    public int source;
    @SerializedName("post_url")
    public String sourceUrl;
    public int date;
    @SerializedName("post_id")
    public int id;
    public boolean favorite;
    public int likes;
    public String text;
    public List<Attachment> attachments;


}
