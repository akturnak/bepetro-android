package com.bepetro.android.data.model;

import android.support.annotation.NonNull;

public class Petro implements Comparable<Petro> {

    public Profile profile;

    @Override
    public int compareTo(@NonNull Petro another) {
        return profile.name.compareToIgnoreCase(another.profile.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Petro petro = (Petro) o;

        return profile != null ? !profile.equals(petro.profile) : petro.profile != null;

    }

    @Override
    public int hashCode() {
        int result = profile != null ? profile.hashCode() : 0;
        result = 31 * result;
        return result;
    }
}
