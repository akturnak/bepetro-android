package com.bepetro.android.data.model;

import java.io.Serializable;

/**
 * Interface for post's attachment.
 */
public interface Attachment extends Serializable {

}
