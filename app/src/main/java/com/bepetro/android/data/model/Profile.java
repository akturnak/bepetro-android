package com.bepetro.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Profile implements Parcelable {
    public String name;
    public String email;
    public String avatar;
    public Date dateOfBirth;

    public Profile() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        return email != null ? !email.equals(profile.email) : profile.email != null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.avatar);
        dest.writeLong(dateOfBirth != null ? dateOfBirth.getTime() : -1);
    }

    protected Profile(Parcel in) {
        this.name = in.readParcelable(Name.class.getClassLoader());
        this.email = in.readString();
        this.avatar = in.readString();
        long tmpDateOfBirth = in.readLong();
        this.dateOfBirth = tmpDateOfBirth == -1 ? null : new Date(tmpDateOfBirth);
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
