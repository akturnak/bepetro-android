package com.bepetro.android.data;


import com.bepetro.android.data.local.DatabaseHelper;
import com.bepetro.android.data.local.PreferencesHelper;
import com.bepetro.android.data.model.Petro;
import com.bepetro.android.data.remote.BePetroService;
import com.bepetro.android.data.remote.BePetroService.SignInResponse;
import com.bepetro.android.data.remote.Const;
import com.bepetro.android.util.EventPosterHelper;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Func1;
import timber.log.Timber;

@Singleton
public class DataManager {

    private final BePetroService mBePetroService;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final EventPosterHelper mEventPoster;


    @Inject
    public DataManager(BePetroService bePetroService,
                       DatabaseHelper databaseHelper,
                       PreferencesHelper preferencesHelper,
                       EventPosterHelper eventPosterHelper) {
        mBePetroService = bePetroService;
        mDatabaseHelper = databaseHelper;
        mPreferencesHelper = preferencesHelper;
        mEventPoster = eventPosterHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    /**
     * 1. Sign in bepetro.com backend
     * 2. If success, save API access token in preferences
     */
    public Observable<Petro> signIn() {
        UUID uuid = UUID.randomUUID(); // generate unique UUID for client
        String randomUUIDString = uuid.toString();
        return mBePetroService.signIn(Const.ANDROID_CLIENT_ID, randomUUIDString)
                .map(new Func1<SignInResponse, Petro>() {
                    @Override
                    public Petro call(SignInResponse signInResponse) {
                        mPreferencesHelper.putAccessToken(signInResponse.accessToken);
                        Petro petro = new Petro();
                        return petro;
                    }
                });

    }

    public Observable<Void> signOut() {
        return mDatabaseHelper.clearTables()
                .doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        mPreferencesHelper.clear();
                        mEventPoster.postEventSafely(new BusEvent.UserSignedOut());
                    }
                });
    }

    public Observable<BePetroService.PostResponse> getPosts(int count) {
        String filters = getPreferencesHelper().getPostFilterString();
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getPosts(auth, count, filters).map(new Func1<BePetroService.PostResponse, BePetroService.PostResponse>() {
            @Override
            public BePetroService.PostResponse call(BePetroService.PostResponse response) {
                if (response.posts == null || response.posts.isEmpty())
                    return response;
                if (mPreferencesHelper.getLastPostDate() < response.posts.get(0).date)
                    mPreferencesHelper.putLastPostDate(response.posts.get(0).date);
                return response;
            }
        });
    }

    public Observable<BePetroService.PostResponse> getMorePosts(int startFrom, int count) {
        String filters = getPreferencesHelper().getPostFilterString();
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getPosts(auth, count, filters, startFrom).map(new Func1<BePetroService.PostResponse, BePetroService.PostResponse>() {
            @Override
            public BePetroService.PostResponse call(BePetroService.PostResponse response) {
                if (response.posts == null || response.posts.isEmpty())
                    return response;
                if (mPreferencesHelper.getLastPostDate() < response.posts.get(0).date)
                    mPreferencesHelper.putLastPostDate(response.posts.get(0).date);
                return response;
            }
        });
    }

    public Observable<BePetroService.PostResponse> getNewPosts(int count, int startTime, Integer startFrom) {
        startTime++; // for not include post with this timestamp (it's must be already in the list)
        String filters = getPreferencesHelper().getPostFilterString();
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getPosts(auth, count, filters, startFrom, startTime).map(new Func1<BePetroService.PostResponse, BePetroService.PostResponse>() {
            @Override
            public BePetroService.PostResponse call(BePetroService.PostResponse response) {
                if (response.posts == null || response.posts.isEmpty())
                    return response;
                if (mPreferencesHelper.getLastPostDate() < response.posts.get(0).date)
                    mPreferencesHelper.putLastPostDate(response.posts.get(0).date);
                return response;
            }
        });
    }

    public Observable<BePetroService.LikeResponse> setFavoritePost(Integer postId) {
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.setFavoritePost(auth, postId);
    }

    public Observable<BePetroService.LikeResponse> deleteFavoritePost(Integer postId) {
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.deleteFavoritePost(auth, postId);
    }

    //  Helper method to post events from doOnCompleted.
    private Action0 postEventSafelyAction(final Object event) {
        return new Action0() {
            @Override
            public void call() {
                mEventPoster.postEventSafely(event);
            }
        };
    }

    public Observable<BePetroService.PostResponse> getFavorites(int count) {
        String filters = getPreferencesHelper().getPostFilterString();
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getFavorites(auth, count, filters);
    }

    public Observable<BePetroService.PostResponse> getMoreFavorites(int offset, int count) {
        String filters = getPreferencesHelper().getPostFilterString();
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getMoreFavorites(auth, count, filters, offset);
    }

    public Observable<Boolean> isNewPost() { // todo: set filters
        String auth = BePetroService.Util.buildAuthorization(mPreferencesHelper.getAccessToken());
        return mBePetroService.getLastPostDate(auth, null).map(new Func1<BePetroService.LastPostDateResponse, Boolean>() {
            @Override
            public Boolean call(BePetroService.LastPostDateResponse response) {
                return mPreferencesHelper.getLastPostDate() < response.date ? Boolean.TRUE : Boolean.FALSE;
            }
        });
    }
}
