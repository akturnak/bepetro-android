package com.bepetro.android.data.model;


/**
 * Attachment for photo post.
 */
public class PhotoAttachment implements Attachment {
    public String srcBig;
    public String srcXBig;
    public String srcXXBig;
}
