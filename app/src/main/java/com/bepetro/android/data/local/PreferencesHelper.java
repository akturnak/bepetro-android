package com.bepetro.android.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.bepetro.android.injection.ApplicationContext;
import com.estimote.sdk.repackaged.gson_v2_3_1.com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "bepetro_app_pref_file";

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static final String PREF_POST_FILTER = "PREF_POST_FILTER";

    private static final String PREF_LAST_POST_DATE = "PREF_LAST_POST_DATE";

    private final SharedPreferences mPref;
    private final Gson mGson;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        mGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz")
                .create();
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    public void putAccessToken(String accessToken) {
        mPref.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    public int getLastPostDate() {
        return mPref.getInt(PREF_LAST_POST_DATE, 0);
    }

    public void putLastPostDate(int lastDate) {
        mPref.edit().putInt(PREF_LAST_POST_DATE, lastDate).apply();
    }

    @Nullable
    public String getAccessToken() {
        return mPref.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    public void putPostFilter(Integer[] filters) {
        String json = mGson.toJson(filters);
        mPref.edit().putString(PREF_POST_FILTER, json).apply();
    }

    @Nullable
    public String getPostFilterString() {
        System.out.println("getPostFilterString !!!");
        Integer[] filters = getPostFilterIndices();
        System.out.println("getPostFilterString.filters.length " + filters.length);
        String resultFilter = "";
        for (Integer i : filters) {
            switch (i) {
                case 0:
                    resultFilter += "," + "post";
                    break;
                case 1:
                    resultFilter += "," + "long_post";
                    break;
                case 2:
                    resultFilter += "," + "photo";
                    break;
            }
        }
        if (resultFilter.startsWith(","))
            resultFilter = resultFilter.substring(1, resultFilter.length());
        System.out.println("resultFilter::: " + resultFilter);
        return resultFilter;
    }


    public Integer[] getPostFilterIndices() { // todo: Привязать индексы к положению названий фильтров в текстовом массиве
        Integer[] defaultFilters = {0, 1, 2};
        String json = mPref.getString(PREF_POST_FILTER, null);
        if (json == null) {
            return defaultFilters;
        } else {
            Type type = new TypeToken<ArrayList<Integer>>() {
            }.getType();
            ArrayList<Integer> filtersList = mGson.fromJson(json, type);
            return filtersList.toArray(new Integer[filtersList.size()]);
        }
    }

}
