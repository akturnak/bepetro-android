package com.bepetro.android.data.model;

/**
 * Attachment for long_post.
 */
public class LongPostAttachment implements Attachment {
    public String text;
}
