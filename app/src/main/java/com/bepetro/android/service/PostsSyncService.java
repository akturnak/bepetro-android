package com.bepetro.android.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import javax.inject.Inject;

import com.bepetro.android.BePetroApplication;
import com.bepetro.android.data.DataManager;
import com.bepetro.android.data.remote.BePetroService;
import com.bepetro.android.util.AndroidComponentUtil;
import com.bepetro.android.util.NetworkUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class PostsSyncService extends Service {

    @Inject DataManager mDataManager;

    private Subscription mSubscription;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, PostsSyncService.class);
    }

    public static boolean isRunning(Context context) {
        return AndroidComponentUtil.isServiceRunning(context, PostsSyncService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BePetroApplication.get(this).getComponent().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        Timber.i("Starting sync...");

        if (!NetworkUtil.isNetworkConnected(this)) {
            Timber.i("Sync canceled, connection not available");
            AndroidComponentUtil.toggleComponent(this, SyncOnConnectionAvailable.class, true);
            stopSelf(startId);
            return START_NOT_STICKY;
        }
/*
        if (mSubscription != null && !mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        mSubscription = mDataManager.getMorePosts(22222, 50) // todo: think where save startFrom parameter, and default posts count
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BePetroService.PostResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BePetroService.PostResponse postResponse) {

                    }
                });*/

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mSubscription != null) mSubscription.unsubscribe();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static class SyncOnConnectionAvailable extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkUtil.isNetworkConnected(context)) {
                Timber.i("Connection is now available, triggering sync...");
                AndroidComponentUtil.toggleComponent(context, this.getClass(), false);
                context.startService(getStartIntent(context));
            }
        }
    }

}
