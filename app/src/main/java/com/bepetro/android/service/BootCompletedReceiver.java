package com.bepetro.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import com.bepetro.android.data.DataManager;

/**
 * Starts PostSyncService on boot completed.
 */
public class BootCompletedReceiver extends BroadcastReceiver {

    @Inject
    DataManager mDataManager;

    @Override
    public void onReceive(Context context, Intent intent) { // todo: should we notify user about new posts, when app not active?
        /*BePetroApplication.get(context).getComponent().inject(this);
        if (mDataManager.getPreferencesHelper().getAccessToken() != null) {
            context.startService(PostsSyncService.getStartIntent(context));
        }*/
    }

}
